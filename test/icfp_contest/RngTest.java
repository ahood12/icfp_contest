package icfp_contest;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by igor on 8/7/15.
 */

public class RngTest {
    @Test
    public void testRng() {
        final Rng rng = new Rng(17);

        int[] vals = new int[]{ 0, 24107,16552,12125,9427,13152,21440,3383,6873,16117};
        for (int val : vals) {
            assertEquals(val, rng.next());
        }
    }

}
