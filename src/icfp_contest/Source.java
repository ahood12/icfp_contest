package icfp_contest;

import java.util.Iterator;
import java.util.List;

public class Source implements Iterator<Unit> {
    final List<Unit> units;
    int numLeft;
    final Rng rng;

    public Source(List<Unit> units, int numLeft, long seed) {
        this.units = units;
        this.numLeft = numLeft;
        this.rng = new Rng(seed);
    }

    @Override
    public boolean hasNext() {
        return numLeft > 0;
    }

    public Unit next() {
        if (!hasNext()) {
            return null;
        }

        int ix = rng.next() % units.size();
        numLeft--;
        return units.get(ix);
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    public Source nextSource() {
        return new Source(units, numLeft, rng.seed);
    }
}
