package icfp_contest;

import java.util.ArrayList;
import java.util.List;

public class Board {

	final int width;
	final int height;
	
	final List<boolean[]> filled;
	
	public Board(int height, int width) {
        this.height = height;
        this.width = width;
        filled = new ArrayList<boolean[]>(height);
        for (int i = 0; i < height; i++) {
            filled.add(newRow());
        }
	}

    public Board(int height, int width, List<boolean[]> filled) {
        this.height = height;
        this.width = width;
        this.filled = filled;
    }

    public Board clone() {
        List<boolean[]> nf = new ArrayList<>(filled.size());
        for (boolean[] row : filled) {
            nf.add(row.clone());
        }
        return new Board(height, width, nf);
    }

    private boolean[] newRow() {
        return new boolean[width];
    }

    public void markFilled(int x, int y) {
        filled.get(y)[x] = true;
    }

    private boolean isFull(boolean[] row) {
        for (int i = 0; i < row.length; i++) {
            if(!row[i]) return false;
        }
        return true;
    }

    public void removeRow(int y) {
        filled.remove(y);
        filled.add(0, newRow());
    }

    /**
     * Removes filled rows, returns number of rows removed
     */
    public int clearFilledRows() {
        int n = 0;
        for (int i = 0, r = 0; i < height; i++) {
            if(isFull(filled.get(r))) {
                n++;
                removeRow(r);
            } else {
                r++;
            }
        }
        return n;
    }

    public boolean isFilled(int x, int y) {
        return filled.get(y)[x];
    }

    public boolean validUnitPosition(Unit unit) {
        for (Cube e : unit.resolveMembers()) {
            int x = e.col(), y = e.row();
            if(x < 0 || x >= width
                    || y < 0 || y >= height
                    || isFilled(x, y)) {
                return false;
            }
        }
        return true;
    }

    public void lockUnit(Unit unit) {
        for (Cube e : unit.resolveMembers()) {
            int x = e.col(), y = e.row();
            markFilled(x, y);
        }
    }
}
