package icfp_contest;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

import java.util.ArrayList;
import java.util.List;

public class Unit {
    final Cube pivot;
    final List<Cube> members;

    public Unit(Cube pivot, List<Cube> members) {
        this.pivot = pivot;
        this.members = members;
    }

    public Unit move(Cube move) {
        return new Unit(pivot.add(move), members);
    }

    public Unit rotate(boolean clockwise) {
        List<Cube> rotated = new ArrayList<>(members.size());
        for (Cube m : members) {
            rotated.add(m.rotate(clockwise));
        }
        return new Unit(pivot, rotated);
    }

    public Iterable<Cube> resolveMembers() {
        return Iterables.transform(members, new Function<Cube, Cube>() {
            @Override
            public Cube apply(Cube m) {
                return m.add(pivot);
            }
        });
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Unit unit = (Unit) o;

        if (!members.equals(unit.members)) return false;
        if (!pivot.equals(unit.pivot)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = pivot.hashCode();
        result = 31 * result + members.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Unit{" +
                "pivot=" + pivot +
                ", members=" + members +
                '}';
    }
}
