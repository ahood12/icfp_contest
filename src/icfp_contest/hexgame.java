package icfp_contest;

/* This is a companion class to hexgame.java. It handles all of the mechanics related to hexagon grids. */

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**********************************
  This is the main class of a Java program to play a game based on hexagonal tiles.
  The mechanism of handling hexes is in the file hexmech.java.
  Written by: M.H.
  Date: December 2012
 ***********************************/

public class hexgame
{
	Unit unit;
	Source source;
	final Board gameBoard;
	boolean flagUnitIsLocked=false;
	
  public hexgame(Board gb, Source source) {
	  	gameBoard=gb;
	  	this.source=source;
		initGame();
		unit=source.next();
		
		createAndShowGUI();
	}

	public static void start(final Board board, final Source source)
	{
		SwingUtilities.invokeLater(new Runnable() {
				public void run() {
				new hexgame(board,source);
				}
				});
	}

	//constants and global variables
	final static Color COLOURBACK =  Color.WHITE;
	final static Color COLOURCELL =  Color.ORANGE;	 
	final static Color COLOURGRID =  Color.BLACK;
	final static Color REDCOLOR =  Color.RED;	 
	final static Color GREENCOLOR =  Color.GREEN;
	final static Color YELLOWCOLOR =  Color.YELLOW;
	final static Color MAGENTACOLOR =  Color.MAGENTA;
	final static Color COLOURONE = new Color(255,255,255,200);
	final static Color COLOURONETXT = Color.BLUE;
	final static Color COLOURTWO = new Color(0,0,0,200);
	final static Color COLOURTWOTXT = new Color(255,100,255);
	final static int EMPTY = 0;
	final static int HEXSIZE = 60;	//hex size in pixels
	final static int BORDERS = 15;  
	final static int SCRSIZE = HEXSIZE * (50 + 1) + BORDERS*3; //screen size (vertical dimension).

	int[][] board;

	void initGame(){
		board= new int[gameBoard.height][gameBoard.width];
		hexmech.setXYasVertex(false); //RECOMMENDED: leave this as FALSE.

		hexmech.setHeight(HEXSIZE); //Either setHeight or setSize must be run to initialize the hex
		hexmech.setBorders(BORDERS);

		for (int i=0;i<gameBoard.height;i++) {
			for (int j=0;j<gameBoard.width;j++) {
				board[i][j]=EMPTY;
			}
		}
	}

	
	private void createAndShowGUI()
	{
		DrawingPanel panel = new DrawingPanel();


		//JFrame.setDefaultLookAndFeelDecorated(true);
		JFrame frame = new JFrame("Hex Testing 4");
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		Container content = frame.getContentPane();
		content.add(panel);
		//this.add(panel);  -- cannot be done in a static context
		//for hexes in the FLAT orientation, the height of a 10x10 grid is 1.1764 * the width. (from h / (s+t))
		frame.setSize( (int)(SCRSIZE/1.23), SCRSIZE);
		frame.setResizable(false);
		frame.setLocationRelativeTo( null );
		frame.setVisible(true);
	}


	class DrawingPanel extends JPanel
	{		
		//mouse variables here
		//Point mPt = new Point(0,0);

		public DrawingPanel()
		{	
			markUnit(unit);
			setBackground(COLOURBACK);
			this.setFocusable(true);
			this.requestFocusInWindow();
			MyKeyListener ml = new MyKeyListener();            
			addKeyListener(ml);
		}

		public void markClear(int i, int j){
			board[i][j] = (int)'C';
		}
		public void markFilled(int i, int j){
			board[i][j] = (int)'F';
		}
		
		public void markUnit(Unit U){
			//board[i][j] = (int)'F';
			for(Cube cube:U.resolveMembers()){
				int col = cube.col();
				int row = cube.row();
				board[row][col]=(int) 'U';
			}
			int pivotRow = U.pivot.row();
			int pivotCol = U.pivot.col();
			if ((pivotRow >=0)&&(pivotRow < gameBoard.height)&&(pivotCol >= 0)&&(pivotCol < gameBoard.width))
				board[pivotRow][pivotCol]=(int) 'P';
		}
		
		public void markVisited(Unit U){
			
			for(Cube cube:U.resolveMembers()){
				int col = cube.col();
				int row = cube.row();
				board[row][col]=(int) 'V';
			}
			int pivotRow = U.pivot.row();
			int pivotCol = U.pivot.col();
			if ((pivotRow >=0)&&(pivotRow < gameBoard.height)&&(pivotCol >= 0)&&(pivotCol < gameBoard.width))
				if (board[pivotRow][pivotCol]=='P')
					board[pivotRow][pivotCol] = (int) 'Y';
		}
		public void clearVisited(Unit U){
			for (int i=0;i<gameBoard.height;i++) {
				for (int j=0;j<gameBoard.width;j++) {	
				if ((board[i][j]=='V')||(board[i][j]=='P'))
				board[i][j]='Y';
			}
			}
			int pivotRow = U.pivot.row();
			int pivotCol = U.pivot.col();
			if ((pivotRow >=0)&&(pivotRow < gameBoard.height)&&(pivotCol >= 0)&&(pivotCol < gameBoard.width))
				board[pivotRow][pivotCol]=(int) 'Y';

			for(Cube cube:U.resolveMembers()){
				int col = cube.col();
				int row = cube.row();
				board[row][col]=(int) 'F';
			}
			
			
		}
		public void paintComponent(Graphics g)
		{
			Graphics2D g2 = (Graphics2D)g;
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setFont(new Font("TimesRoman", Font.PLAIN, 20));
			super.paintComponent(g2);
			//draw grid
			for (int i=0;i<gameBoard.height;i++) {
				for (int j=0;j<gameBoard.width;j++) {
					hexmech.drawHex(i,j,g2);
				}
			}
			//fill in hexes
			for (int i=0;i<gameBoard.height;i++) {
				for (int j=0;j<gameBoard.width;j++) {	
	
					//if (board[i][j] < 0) hexmech.fillHex(i,j,COLOURONE,-board[i][j],g2);
					//if (board[i][j] > 0) hexmech.fillHex(i,j,COLOURTWO, board[i][j],g2);
					hexmech.fillHex(i,j,board[i][j],g2);
				}
				
			}
			//marrkUnit(unit);
			//g.setColor(Color.RED);
		//g.drawLine(mPt.x,mPt.y, mPt.x,mPt.y);
		}

		class MyKeyListener extends KeyAdapter	{	//inner class inside DrawingPanel 
			public void keyPressed(KeyEvent e) { 
				char pressedChar = e.getKeyChar(); 
				
				Unit newUnit;
				if(pressedChar=='l'){
					gameBoard.lockUnit(unit);
					clearVisited(unit);
					System.out.println("GrabbingNext:");
					unit=source.next();
					System.out.println(unit.toString());
					markUnit(unit);
					//flagUnitIsLocked=true;
					repaint();
					return;
					
				}
				//flag=false;
				if(pressedChar=='q'){
					newUnit = unit.move(Cube.W);
				}else if(pressedChar=='e'){
					newUnit = unit.move(Cube.E);
				}else if(pressedChar=='d'){
					newUnit = unit.move(Cube.SE);
				}else if(pressedChar=='a'){
					newUnit = unit.move(Cube.SW);
				}else if(pressedChar=='['){
					newUnit = unit.rotate(true);
				}else if(pressedChar==']'){
					newUnit = unit.rotate(false);
				}else
				{ return;}
				if(gameBoard.validUnitPosition(newUnit)){
				markVisited(unit);
				unit=newUnit;
				markUnit(newUnit);
				}else{
					gameBoard.lockUnit(unit);
					System.out.println("GrabbingNext:");
					clearVisited(unit);
					//repaint();
					if (source.numLeft > 0)
					{
						unit=source.next();
						System.out.println(unit.toString());
						markUnit(unit);
					}
					else
					{
						int score = 0;
						for (int i=0;i<gameBoard.height;i++) {
							for (int j=0;j<gameBoard.width;j++) 
								if (board[i][j]=='Y'){	
									score++;
								}
							}
						JOptionPane.showMessageDialog(null,"SCORE = " + score,"END OF FUN",JOptionPane.WARNING_MESSAGE);
					}
					repaint();
					return;
				}
				

				//DEBUG: colour in the hex which is supposedly the one clicked on
				//clear the whole screen first.
				/* for (int i=0;i<BSIZE;i++) {
					for (int j=0;j<BSIZE;j++) {
						board[i][j]=EMPTY;
					}
				} */

				//What do you want to do when a hexagon is clicked?
				//board[p.x][p.y] = (int)'X';
				repaint();
			}		 
		} //end of MyMouseListener class 
	} // end of DrawingPanel class
}