package icfp_contest;

public class Rng {
    private static final long MUL = 1103515245;
    private static final long INCREMENT = 12345;
    private static final long MASK =((1L << 31) - 1);

    public long seed;

    public Rng(long seed) {
        this.seed = seed;
    }

    public int next() {
        final int v = (int) (seed& MASK)>>>16;
        seed = (seed * MUL + INCREMENT) & MASK;
        return v;
    }
}
