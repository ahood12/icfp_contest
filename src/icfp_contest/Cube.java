package icfp_contest;

/**
 * Created by igor on 8/7/15.
 */
public class Cube implements Cloneable {
    final int x, y, z;

    public Cube(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Cube(int col, int row) {
        x = col - (row - (row&1)) / 2;
        z = row;
        y = -x-z;
    }

    public Cube clone() {
       return new Cube(x, y, z);
    }

    public int row() {
        return z;
    }

    public int col() {
        return x + (z - (z&1)) / 2;
    }
    
    public Cube add(Cube c) {
        return new Cube(this.x + c.x, this.y + c.y, this.z + c.z);
    }
    
    public Cube sub(Cube c) {
        return new Cube(this.x - c.x, this.y - c.y, this.z - c.z);
    }

    public Cube rotate(boolean clockwise) {
        if(clockwise) {
            return new Cube(-z, -x, -y);
        } else {
            return new Cube(-y, -z, -x);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cube cube = (Cube) o;

        if (x != cube.x) return false;
        if (y != cube.y) return false;
        if (z != cube.z) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        result = 31 * result + z;
        return result;
    }

    @Override
    public String toString() {
        return "{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                "}: [" + col() + ',' + row() + ']';
    }

    public static final Cube W = new Cube(-1, 1, 0);
    public static final Cube E = new Cube(1, -1, 0);
    public static final Cube SW = new Cube(-1, 0, 1);
    public static final Cube SE = new Cube(0, -1, 1);
}
