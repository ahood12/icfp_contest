package icfp_contest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class Games {
	static final String keyId = "id";
	static final String keyHeight = "height";
	static final String keyWidth = "width";
	static final String keySourceSeeds = "sourceSeeds";
	static final String keyUnits = "units";
	static final String keyFilled = "filled";
	static final String keySourceLength = "sourceLength";
	static final String keyMembers = "members";
	static final String keyPivot = "pivot";
	
	final Board initBoard;
	final List<Unit> units;
	final int sourceLength;
	final int gameId;
    final int[] seeds;

	public Games(String pathToJsonFile) throws Exception {
        JSONObject jsonAll = new JSONObject(new JSONTokener(new FileReader(pathToJsonFile)));

        gameId = jsonAll.getInt(keyId); // Will be needed in the output
        sourceLength = jsonAll.getInt(keySourceLength);// how many unit

        // initialize the board
        int height = jsonAll.getInt(keyHeight);
        int width = jsonAll.getInt(keyWidth);

        initBoard = new Board(height, width);

        JSONArray filled = jsonAll.getJSONArray(keyFilled);
        for (int i = 0; i < filled.length(); i++) {
            JSONObject filledCell = (JSONObject) filled.get(i);
            initBoard.markFilled(filledCell.getInt("y"), filledCell.getInt("x"));
        }

        // extract the units
        Cube startPoint = new Cube(width / 2, 0);
        JSONArray unitsArr = jsonAll.getJSONArray(keyUnits);
        units = new ArrayList<Unit>(unitsArr.length());
        for (int i = 0; i < unitsArr.length(); i++) {
            JSONObject unit = (JSONObject) unitsArr.get(i);
            JSONArray memberArr = unit.getJSONArray(keyMembers);
            JSONObject pivotObj = unit.getJSONObject(keyPivot);

            int minCol = 0, maxCol = 0, minRow = 0, maxRow = 0;
            Cube pivot = new Cube(pivotObj.getInt("x"), pivotObj.getInt("y"));

            List<Cube> members = new ArrayList<>(memberArr.length());
            for (int j = 0; j < memberArr.length(); j++) {
                JSONObject member = (JSONObject) memberArr.get(j);
                int col = member.getInt("x");
                int row = member.getInt("y");
                if(row < minRow) {
                    minRow = row;
                }
                if(row > maxRow) {
                    maxRow = row;
                }
                if(col < minCol) {
                    minCol = col;
                }
                if(col> maxCol) {
                    maxCol = col;
                }
                Cube e = new Cube(col, row);
                members.add(e.sub(pivot));
            }
            int uw = maxCol - minCol;

            Cube ref = new Cube(uw / 2+1, minRow);
            Cube initPivot = pivot.sub(ref).add(startPoint);

            units.add(new Unit(initPivot, members));
        }

        JSONArray seedArr = jsonAll.getJSONArray(keySourceSeeds);
        seeds = new int[seedArr.length()];
        for(int i = 0; i < seedArr.length(); i++) {
            seeds[i] = seedArr.getInt(i);
        }
    }

    public List<Source> sources() {
        List<Source> sources = new ArrayList<Source>(seeds.length);
        for (int seed : seeds) {
            sources.add(new Source(units, sourceLength, seed));
        }
        return sources;
    }

	public static void main(String[] args) throws Exception {
        Games games = new Games("problem_0.json");

        for (Source source : games.sources()) {
            Board board = games.initBoard.clone();
            solve(board, source);
        }
    }

    private static void solve(Board board, Source source) {
    	hexgame showBoard= new hexgame(board,source);
    	//showBoard.start(board,source);
    }
}
